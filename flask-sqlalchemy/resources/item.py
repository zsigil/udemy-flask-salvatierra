from flask_restful import Resource, reqparse
from flask_jwt import jwt_required
from models.item import ItemModel


class Item(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('price', type=float, required=True,help="This field cannot be blank")
    parser.add_argument('store_id', type=int, required=True,help="Every item needs a store id")

    @jwt_required()
    def get(self, name):
        item = ItemModel.find_by_name(name)
        if item:
            return item.json() #because we cannot return object
        return {'message': 'Item not found'}, 404

    def post(self, name):
        #check if item with this name already exists
        #if error is found, we do not proceed
        if ItemModel.find_by_name(name):
            return {'message': 'Item already exists'}, 400
        
        request_data = Item.parser.parse_args()

        item = ItemModel(name, request_data['price'], request_data['store_id'] )
        
        try:
            item.save_to_db()
        except:
            return {'message': 'An error occurred with inserting item into table'}, 500
        
        return item.json(),201

    def delete(self,name):
        item = ItemModel.find_by_name(name)

        if not item:
            return {'message': 'Item does not exist'}, 400

        item.delete_from_db()
        return {'message': 'Item deleted'}

    def put(self,name):
        # put updates object if it exists, creates if not

        item = ItemModel.find_by_name(name)
        request_data = Item.parser.parse_args()
        
        if item:
            #item already exists
            try:
                item.price = request_data['price']
            except:
                return {'message': 'Error updating item'}, 500

        else:
            #item does not exist
            try:
                item = ItemModel(name,request_data['price'], request_data['store_id'])    
            except:
                return {'message': 'Error creating item'}, 500
        
        item.save_to_db()
        return item.json()


class ItemList(Resource):
    def get(self):
        return {'items':[item.json() for item in ItemModel.query.all()]}  #always has to return a dictionary!!!