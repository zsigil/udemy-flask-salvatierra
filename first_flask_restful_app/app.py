from flask import Flask, request
from flask_restful import Resource, Api, reqparse
from flask_jwt import JWT, jwt_required

from security import authenticate, identity

app = Flask(__name__)
api = Api(app)

app.secret_key = 'secretkey'

jwt = JWT(app, authenticate, identity)  #/auth

items = []

class Item(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('price', type=float, required=True,help="This field cannot be blank")

    @jwt_required()
    def get(self, name):
        #filter returns filter object,has to be turned to a list
        #item = list(filter(lambda x: x['name']==name, items))  

        #next gives first item, if called again, gives second, etc
        #if no items(left), next will raise an error, so we need 'None'
        item = next(filter(lambda x: x['name']==name, items), None)  

        return {'item':item}, 200 if item else 404
    
    def post(self, name):
        #check if item with this name already exists
        #if error is found, we do not proceed
        if next(filter(lambda x: x['name']==name, items), None):
            return {'message': 'Item already exists'}, 400

        request_data = Item.parser.parse_args()
        
        item = {'name':name, 'price':request_data['price']}
        items.append(item)
        return item,201

    def delete(self,name):
        global items #else another 'items' is created and exists only in this delete scope 
        items = list(filter(lambda x: x['name'] != name, items))
        return {'message': 'Item deleted'}

    def put(self,name):
        # put updates object if it exists, creates if not

        item = next(filter(lambda x: x['name']==name, items), None)

        request_data = Item.parser.parse_args()
        
        if item:
            #item already exists, find and update it
            item.update(request_data)
        else:
            #item does not exist
            item = {'name':name, 'price':request_data['price']}
            items.append(item)
        
        return item


class ItemList(Resource):
    def get(self):
        return {'items':items}  #always has to return a dictionary!!!


api.add_resource(Item, '/item/<string:name>') 
api.add_resource(ItemList, '/items')

app.run(port=5000, debug=True)