from flask import Flask

app = Flask(__name__)

@app.route('/')  #by default, this will be a GET request
def home():
    return 'Hello, world'


app.run(port=5000)