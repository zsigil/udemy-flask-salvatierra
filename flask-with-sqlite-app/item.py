import sqlite3
from sqlite3.dbapi2 import connect
from flask_restful import Resource, reqparse
from flask_jwt import jwt_required


class Item(Resource):
    parser = reqparse.RequestParser()
    parser.add_argument('price', type=float, required=True,help="This field cannot be blank")

    @jwt_required()
    def get(self, name):
        item = Item.find_by_name(name)
        if item:
            return item
        return {'message': 'Item not found'}, 404
    

    @classmethod
    def find_by_name(cls,name):
        connection = sqlite3.connect('data.db')
        cursor=connection.cursor()

        query = "SELECT * FROM items WHERE name=?"
        result = cursor.execute(query, (name,))
        row = result.fetchone()

        connection.close()
        
        if row:   
            return {'item': {'name':row[0], 'price':row[1]}}, 200
        
        #returns None as default
        


    def post(self, name):
        #check if item with this name already exists
        #if error is found, we do not proceed
        if Item.find_by_name(name):
            return {'message': 'Item already exists'}, 400
        
        request_data = Item.parser.parse_args()
        
        item = {'name':name, 'price':request_data['price']}
        
        try:
            Item.insert_item(item)
        except:
            return {'message': 'An error occurred with inserting item into table'}, 500
        
        return item,201
        

    @classmethod
    def insert_item(cls,item):
        connection = sqlite3.connect('data.db')
        cursor = connection.cursor()
        query = "INSERT INTO items VALUES (?, ?)"
        cursor.execute(query, (item['name'], item['price']))
        connection.commit()
        connection.close()
    
    @classmethod
    def update_item(cls,item):
        connection = sqlite3.connect('data.db')
        cursor = connection.cursor()
        query = "UPDATE items SET price=? WHERE name=?"
        cursor.execute(query, (item['price'], item['name']))
        connection.commit()
        connection.close()


    def delete(self,name):
        if not Item.find_by_name(name):
            return {'message': 'Item does not exist'}, 400

        connection = sqlite3.connect('data.db')
        cursor = connection.cursor()

        query = "DELETE FROM items WHERE name=?"
        cursor.execute(query, (name,))
        connection.commit()
        connection.close()

        return {'message': 'Item deleted'}


    def put(self,name):
        # put updates object if it exists, creates if not

        item = Item.find_by_name(name)
        request_data = Item.parser.parse_args()
        updated_item = {'name': name, 'price':request_data['price']}
        
        if item:
            #item already exists, update it
            try:
                Item.update_item(updated_item)
            except:
                return {'message': 'Error updating item'}, 500

        else:
            #item does not exist
            try:
                Item.insert_item(updated_item)
            except:
                return {'message': 'Error creating item'}, 500
        
        return updated_item


class ItemList(Resource):
    def get(self):
        connection = sqlite3.connect('data.db')
        cursor = connection.cursor()

        query = "SELECT * FROM items"
        result = cursor.execute(query)
        items = []
        for row in result:
            item = {'name': row[0], 'price': row[1]}
            items.append(item)

        connection.close()

        return {'items':items}  #always has to return a dictionary!!!